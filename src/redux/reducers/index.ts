import {DATA_LOADING, DATA_FETCHED,DATA_ERROR} from '../actions/fetch';
interface Action {
  type: string;
  payload: any;
}
interface State {
  data: any[];
  errMsg:any
  loading: boolean;
}

const intialState = {
  data: [],
  loading: false,
  errMsg:null
};

export default (state: State = intialState, action: Action) => {
  switch (action.type) {
    case DATA_FETCHED:
      return {
        ...state,
        data: action.payload,
      };
    case DATA_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case DATA_ERROR:
      return {
        ...state,
        errMsg: action.payload,
      };
    default:
      return state;
  }
};
