import {Dispatch} from 'redux';
import {fetchDataService} from 'services/fetchData';
export const DATA_FETCHED = 'DATA_FETCHED';
export const DATA_LOADING = 'DATA_LOADING';
export const DATA_ERROR = 'DATA_ERROR';


export function fetchData() {
  return async (dispatch: Dispatch) => {
    dispatch(loading(true));
    dispatch(getError(null));
    try {
      const response = await fetchDataService();
      if (response.data) {
        dispatch(imageDataFetched(response.data));
        dispatch(loading(false));
      }
    } catch (e) {
      dispatch(getError(e.message));
      dispatch(loading(false));
    }
  };
}
export function fetchDataWithFilter(type: string) {
  return async (dispatch: Dispatch) => {
    dispatch(imageDataFetched([]));
    dispatch(loading(null));
    dispatch(loading(true));
    try {
      const response = await fetchDataService();
      if (response.data) {
        if (type != 'All') {
          let data = response.data;
          let result = data.filter((item) => item.colour == type);
          dispatch(imageDataFetched(result));
          dispatch(loading(false));
          return;
        }
        dispatch(imageDataFetched(response.data));
        dispatch(loading(false));
      }
    } catch (e) {
      dispatch(getError(e.message));
      dispatch(loading(false));
    }
  };
}

const imageDataFetched = (data: any[]) => ({
  type: DATA_FETCHED,
  payload: data,
});

export const loading = (loader: boolean) => ({
  type: DATA_LOADING,
  payload: loader,
});
export const getError = (error: any) => ({
  type: DATA_ERROR,
  payload: error,
});
