import {StyleSheet} from 'react-native';
import {colors, resize} from 'constantsStyle';

export default styles = StyleSheet.create({
  buttonBlock: {
    height: resize.heightScale(45),
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderWidth: 0.5,
    alignItems: 'center',
    width: resize.width / 3,
    paddingHorizontal: resize.widthScale(10),
  },
  buttonText: {
    fontSize: resize.square(16),
  },
  buttonIcon: {
    width: resize.square(15),
    height: resize.square(15),
  },
});
