import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ViewProps,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import styles from './Card.styles';
interface Props extends ViewProps {
  data: any;
  onPressQty?: () => void;
}
interface State {
  Qty: number;
  price: number;
}

export class Card extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      Qty: 0,
      price: 0,
    };
  }
  subQty(num: number, price: number, totalPrice: number) {
    if (num > 0) {
      this.setState({Qty: num - 1, price: totalPrice - price});
      this.props.onPressQty(-price);
    }
  }
  addQty(num: number, price: any, totalPrice: number) {
    this.setState({Qty: num + 1, price: totalPrice + price});
    this.props.onPressQty(price);
  }
  removeQty(num: number, price: any, totalPrice: number) {
    this.props.onPressQty(-totalPrice);
    this.setState({Qty: 0, price: 0});
  }
  render() {
    const {data} = this.props;
    const {Qty, price} = this.state;
    return (
      <View {...this.props} style={styles.itemContainer}>
        <Image source={{uri: data.img}} style={styles.avatarStyle} />
        <View style={styles.centerBlock}>
          <Text numberOfLines={2} style={styles.titleText}>
            {data.name}
          </Text>
          <Text style={styles.priceText}>{'€' + Math.round(data.price)}</Text>
        </View>
        <View style={styles.rightBlock}>
          <TouchableOpacity
            onPress={() => this.subQty(Qty, Math.round(data.price), price)}
            style={styles.addSubBlock}>
            <Text style={styles.addSubText}>{'-'}</Text>
          </TouchableOpacity>
          <View style={styles.removeBlock}>
            <Text style={styles.priceText}>{Qty}</Text>
            <TouchableOpacity
              onPress={() => this.removeQty(Qty, Math.round(data.price), price)}
              style={styles.addSubBlock}>
              <Text style={styles.removeText}>{'remove'}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.addSubBlock}
            onPress={() => this.addQty(Qty, Math.round(data.price), price)}>
            <Text style={styles.addSubText}>{'+'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
