import React, {Component} from 'react';
import {
  View,
  FlatList,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {NavigationScreenProp, NavigationState} from 'react-navigation';
import {connect} from 'react-redux';
import styles from './Home.styles';
import {Card, Button, TryAgain} from 'components';
import {fetchData, fetchDataWithFilter} from 'reduxStore/actions/fetch';

interface Props {
  navigation: NavigationScreenProp<NavigationState>;
  fetchData: () => void;
  fetchDataWithFilter: (type: string) => void;
  imageData: any;
  loading: boolean;
  errMsg: any;
}

interface itemProp {
  item: any;
}
interface State {
  totalPrice: number;
  filterData: Array;
  selectedFilter: string;
  showFilter: boolean;
}

class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      totalPrice: 0,
      showFilter: false,
      selectedFilter: 'All',
      filterData: ['All', 'Black', 'Red', 'Stone'],
    };
  }
  componentDidMount() {
    const {fetchData} = this.props;
    fetchData();
  }
  setTotalPrice(price: number) {
    this.setState({totalPrice: this.state.totalPrice + price});
  }
  selectingFilter(type: string) {
    const {fetchDataWithFilter} = this.props;
    const {selectedFilter} = this.state;
    if (selectedFilter != type) {
      fetchDataWithFilter(type);
      this.setState({showFilter: false, selectedFilter: type, totalPrice: 0});
      return;
    }
    this.setState({showFilter: false});
  }
  reloadData() {
    const {selectedFilter} = this.state;
    const {fetchDataWithFilter, fetchData} = this.props;
    if (selectedFilter == 'All') {
      fetchData();
      return;
    }
    fetchDataWithFilter(selectedFilter);
  }
  render() {
    const {navigation, imageData, loading, errMsg} = this.props;
    const {totalPrice, filterData, selectedFilter, showFilter} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Button
            buttonText={selectedFilter}
            avatar={require('src/assets/icons/dropDown.png')}
            onPress={() => this.setState({showFilter: !showFilter})}
            accessibilityLabel="decrement"
            color="red"
          />
        </View>
        <FlatList
          data={imageData}
          style={styles.flatListBlock}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}: itemProp) => {
            return (
              <Card
                onPressQty={(price) => this.setTotalPrice(price)}
                onPressPlus={() => console.log('press')}
                data={item}
              />
            );
          }}
        />
        <View style={styles.totalBlock}>
          <Text style={styles.totalText}>{'€' + totalPrice}</Text>
        </View>
        {showFilter ? (
          <View style={styles.filterDropDwon}>
            <FlatList
              data={filterData}
              keyExtractor={(i) => i}
              renderItem={({item}: itemProp) => {
                return (
                  <TouchableOpacity
                    onPress={() => this.selectingFilter(item)}
                    style={styles.filterItemBlock}>
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        ) : null}
        {loading ? (
          <View style={styles.loading}>
            <ActivityIndicator
              style={styles.styleComponent}
              color={'grey'}
              size={'large'}
            />
          </View>
        ) : null}
        {errMsg ? (
          <TryAgain
            onPress={() => this.reloadData()}
            show={true}
            msg={errMsg}
          />
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = (state: any) => ({
  imageData: state.data,
  loading: state.loading,
  errMsg: state.errMsg,
});

function bindToAction(dispatch: any) {
  return {
    fetchDataWithFilter: (type: string) => dispatch(fetchDataWithFilter(type)),
    fetchData: () => dispatch(fetchData()),
  };
}

export default connect(mapStateToProps, bindToAction)(Home);
