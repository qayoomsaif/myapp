import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
const scale = (size: number) => (width / 410) * size;
const scaleHeight = (size: number) => (height / 820) * size;

export const resize = {
  width: width,
  height: height,
  heightScale: (size: number) => size + (scaleHeight(size) - size),
  widthScale: (size: number) => size + (scale(size) - size),
  square: (size: number) =>
    height < width
      ? size + (scaleHeight(size) - size)
      : size + (scale(size) - size),
};
