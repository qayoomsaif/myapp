import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {Dimensions} from 'react-native';
import {createDrawerNavigator} from 'react-navigation-drawer';
const {width} = Dimensions.get('window');

import Home from 'screens/Home/Home';
const MainStack = createStackNavigator(
  {
    Home: {screen: Home},
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  },
);

export default createAppContainer(
  createSwitchNavigator(
    {
      MainStack: MainStack,
    },
    {
      initialRouteName: 'MainStack',
    },
  ),
);
