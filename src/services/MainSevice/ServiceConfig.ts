import {requestSend} from './requestSend';
const BaseUrl = {
  api: 'https://my-json-server.typicode.com/',
  pyment: 'https://api.youcan.tech.com/',
};
class Service {
  // Generic GET method
  async GET(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    header: any = {},
    baseUrlType: number = 0,
    contentType: number = 0,
    DoNotUrlEncoded: boolean = false,
  ) {
    var url, setCofig;
    try {
      // receives full url, header and youcan Server request
      setCofig = await this.setCofig(
        api,
        header,
        baseUrlType,
        contentType,
        isLogin,
      );
      url = setCofig.url;
      if (param) {
        encodedData = await this.toFormUrlEncoded(param);
        url = setCofig.url += '?' + encodedData;
      }
      let response = await requestSend(
        url,
        null,
        setCofig.header,
        "GET"
      );
      return response;
    } catch (e) {

      console.log('................',e);
      
      throw e;
    }
  }

  async POST(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    header: any = {},
    baseUrlType: number = 0,
    contentType: number = 0,
    DoNotUrlEncoded: boolean = false,
  ) {
    var body, setCofig;
    try {
      setCofig = await this.setCofig(
        api,
        header,
        baseUrlType,
        contentType,
        isLogin,
      );
      if (DoNotUrlEncoded) {
        body = JSON.stringify(param);
      } else {
        body = await this.toFormUrlEncoded(param);
      }
      let response = await this.serviceMain.POST(
        setCofig.url,
        body,
        setCofig.header,
        setCofig.ouerSevereRequest,
      );
      return response;
    } catch (e) {
      throw e;
    }
  }
  async PATCH(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    header: any = {},
    baseUrlType: number = 0,
    contentType: number = 0,
    DoNotUrlEncoded: boolean = false,
  ) {
    var body, setCofig;
    try {
      setCofig = await this.setCofig(
        api,
        header,
        baseUrlType,
        contentType,
        isLogin,
      );
      if (DoNotUrlEncoded) {
        body = JSON.stringify(param);
      } else {
        body = await this.toFormUrlEncoded(param);
      }
      let response = await this.serviceMain.PATCH(
        setCofig.url,
        body,
        setCofig.header,
        setCofig.ouerSevereRequest,
      );
      return response;
    } catch (e) {
      throw e;
    }
  }
  async Delete(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    header: any = {},
    baseUrlType: number = 0,
    contentType: number = 0,
    DoNotUrlEncoded: boolean = false,
  ) {
    var body, setCofig;
    try {
      setCofig = await this.setCofig(
        api,
        header,
        baseUrlType,
        contentType,
        isLogin,
      );
      if (DoNotUrlEncoded) {
        body = JSON.stringify(param);
      } else {
        body = await this.toFormUrlEncoded(param);
      }
      let response = await this.serviceMain.DeleteNewService(
        setCofig.url,
        body,
        setCofig.header,
        setCofig.ouerSevereRequest,
      );
      return response;
    } catch (e) {
      throw e;
    }
  }

  async PUT(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    header: any = {},
    baseUrlType: number = 0,
    contentType: number = 0,
    DoNotUrlEncoded: boolean = false,
  ) {
    var body, setCofig;
    try {
      setCofig = await this.setCofig(
        api,
        header,
        baseUrlType,
        contentType,
        isLogin,
      );
      if (DoNotUrlEncoded) {
        body = JSON.stringify(param);
      } else {
        body = await this.toFormUrlEncoded(param);
      }
      let response = await this.serviceMain.PUTNewService(
        setCofig.url,
        body,
        setCofig.header,
        setCofig.ouerSevereRequest,
      );
      return response;
    } catch (e) {
      throw e;
    }
  }

  // sets configs e.g header, contentType
  async setCofig(api, header, baseUrlType, contentType, isLogin) {
    // default value for url
    let url = BaseUrl.api + api,
      token,
      Content_Type = 'application/x-www-form-urlencoded'; // default value for Content_Type
    if (contentType == 1) {
      Content_Type = 'application/json';
    }
    var requestHeader = {
      Accept: 'application/json',
      'Content-Type': Content_Type,
    };
    if (header) {
      requestHeader = Object.assign(requestHeader, header);
    }

    // baseUrlType = 1 means it is for payment API for youcan server
    if (baseUrlType === 1) {
      url = this.baseUrl.pyment + api;
    }

    // baseUrlType = 2 means it is for third party server
    if (baseUrlType === 2) {
      url = api;
    }
    return {
      header: requestHeader,
      url: url,
    };
  }

  async toFormUrlEncoded(param) {
    let promise = new Promise(async (resolve, reject) => {
      const formBody = Object.keys(param)
        .map(
          (key) =>
            encodeURIComponent(key) + '=' + encodeURIComponent(param[key]),
        )
        .join('&');
      return resolve(formBody);
    });
    return promise;
  }
}
module.exports = {Service:new Service()};
