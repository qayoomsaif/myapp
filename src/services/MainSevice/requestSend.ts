import {generateApiError, generateNetworkError} from 'customError/genrateError';
export async function requestSend(
  url: string,
  body: any,
  header: any,
  method: string,
) {
  let option = {method: method, headers: header, body: body};
  if (method == 'GET') {
    delete option.body;
  }
  try {
    const response = await fetch(url, option);
    if (response.status == 401) {
      generateApiError(
        null,
        response,
        url,
        'unauthorized',
        response ? response : null,
      );
    }
    if (response.status == 204) {
      return {data: null, message: 'success'};
    }
    const responseJson = await responseToJSon(response, url);
    if (response.status == 403) {
      generateApiError(
        null,
        response,
        url,
        responseJson.error.message ? responseJson.error.message : 'forbidden',
        responseJson.error
          ? responseJson.error
          : responseJson
          ? responseJson
          : null,
      );
    }
    if (response.status == 404) {
      generateApiError(
        null,
        response,
        url,
        responseJson.error.message
          ? responseJson.error.message
          : 'url not found',
        responseJson.error
          ? responseJson.error
          : responseJson
          ? responseJson
          : null,
      );
    }
    if (response.status == 400) {
      generateApiError(
        null,
        response,
        url,
        responseJson.error.message ? responseJson.error.message : 'bad request',
        responseJson.error
          ? responseJson.error
          : responseJson
          ? responseJson
          : null,
      );
    }
    if (response.status == 500) {
      generateApiError(
        null,
        response,
        url,
        responseJson.error.message
          ? responseJson.error.message
          : 'server failed',
        responseJson.error
          ? responseJson.error
          : responseJson
          ? responseJson
          : null,
      );
    }
    if (response.status == 200) {
      if (responseJson.status == 404) {
        generateApiError(
          null,
          response,
          url,
          'url not found',
          responseJson.error
            ? responseJson.error
            : responseJson
            ? responseJson
            : responseJson
            ? responseJson
            : null,
        );
      }
      if (responseJson.response) {
        if (responseJson.response.data) {
          return {
            message: 'success',
            data: responseJson.response.data
              ? responseJson.response.data
              : null,
          };
        }
        return {
          message: 'success',
          data: responseJson.response ? responseJson.response : null,
        };
      }
      return {data: responseJson, message: 'success'};
    }
    generateApiError(null, response, url, responseJson ? responseJson : null);
  } catch (e) {
    console.log(
      ' - --------------------------------------------> GETNewService',
      url,
      '\n ============================> ',
      e,
    );
    if (e.message === 'Network request failed') {
      generateNetworkError(url, e);
    }
    if (!e.mainType) {
      generateApiError(null, null, url, e.message, e ? e : null);
    }
    throw e;
  }
}
export async function responseToJSon(response: any, url: string) {
  try {
    return await response.json();
  } catch (e) {
    generateApiError(
      null,
      response,
      url,
      'json error',
      response ? response : null,
    );
  }
}
